/* This program is a part of a simple hibernate example used for CIT-360
   It is written by Troy Tuckett.  BYUI.EDU
 */
package com.example.finalproject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/** TestDAO implemented using a singleton pattern
 *  Used to get city data from my MYSQL database*/
public class DatabaseFunctions {

    SessionFactory factory = null;
    Session session = null;

    private static DatabaseFunctions single_instance = null;

    private DatabaseFunctions()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static DatabaseFunctions getInstance()
    {
        if (single_instance == null) {
            single_instance = new DatabaseFunctions();
        }

        return single_instance;
    }

    public List<BooksEntity> getBooks(){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "FROM BooksEntity";
            return session.createQuery(query, BooksEntity.class).getResultList();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
        finally {
            session.close();
        }

    }

    public List<CheckedoutEntity> getCheckedOut(){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "FROM CheckedoutEntity ";
            return session.createQuery(query, CheckedoutEntity.class).getResultList();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
        finally {
            session.close();
        }

    }

    public List<ReturnedEntity> getReturns(){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "FROM ReturnedEntity ";
            return session.createQuery(query, ReturnedEntity.class).getResultList();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
        finally {
            session.close();
        }

    }

    public String getBookByID(int bookID){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "FROM BooksEntity WHERE bid = " + bookID;
            BooksEntity book = session.createQuery(query, BooksEntity.class).getSingleResult();
            return book.getBookName();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
            return "";
        }
        finally {
            session.close();
        }

    }
}
