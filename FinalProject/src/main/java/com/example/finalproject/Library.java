package com.example.finalproject;

import java.awt.print.Book;
import java.io.*;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "library", value = "/library")
public class Library extends HttpServlet {


    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        DatabaseFunctions dbF = DatabaseFunctions.getInstance();
        BuildHTML buildHTML = new BuildHTML();
        List<BooksEntity> books = dbF.getBooks();
        List<CheckedoutEntity> checkedOuts = dbF.getCheckedOut();
        List<ReturnedEntity> returns =dbF.getReturns();
        String checkOutForm = buildHTML.buildCheckedOutForm(books, checkedOuts, returns);
        String returnForm = buildHTML.buildReturnsForm(checkedOuts, returns, dbF);

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>Inventory Management</h1>");
        out.println("<h2>List of Available Books</h2>");
        out.println("<ul>");
        for (BooksEntity book:
            books ) {
            out.println("<li>" + book.getBookName() + "</li>");
        }
        out.println("</ul>");
        out.println("<h2>Book Check-Out</h2>");
        out.println(checkOutForm);
        out.println("<h2>Book Returns</h2>");
        out.println(returnForm);
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String action = req.getParameter("action");

        switch (action){
            case "checkOut":

                resp.sendRedirect("library");
                break;
            case "returns":

                resp.sendRedirect("library");
                break;
        }

    }

    public void destroy() {
    }
}