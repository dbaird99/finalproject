<%--
  Created by IntelliJ IDEA.
  User: dbair
  Date: 4/4/2022
  Time: 8:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
<title>Sign Up</title>
    <link rel="stylesheet" type="text/css" href="stylesheet.css"/>
</head>
<body>
<h2>Ogden-Weber Technical College</h2>
<p>Information Technology Program</p>
<br/>
<img src="images/owatc.png" alt="Ogden-Weber Tech Logo" class="center">
<br/>
<h1>Welcome to Sign up</h1>
<fieldset id="signup">
        <legend>Sign Up!</legend>

        <form action="SignUp_Servlet.html" method="post">
          <label class="top">Username*: <input type="text" name="username" required
                                                id="userSign" maxlength="30"    title="create username" ></label>
                <br>
              <!--Password 1-->
              <label class="top">Password*: <input id="pass1" type="password" name="password1" required
                                                       ></label>
              <!-- An element to toggle between password visibility -->
              <input type="checkbox" onclick="showPasswd()">Show Password
                <br>
              <!--Password 2-->
              <label class="top">Confirm Password*: <input id="pass2" type="password" name="password2" required
                                                       ></label>
              <!-- An element to toggle between password visibility -->
              <input type="checkbox" onclick="showPasswd2()">Show Password
                <br>
              <input class="submitBtn-login" type="submit" value="Sign Up!" >
            </form>
</fieldset>

<footer>

</footer>
<script src="js/basic.js"></script>
</body>
</html>
