<%--
  Created by IntelliJ IDEA.
  User: dbair
  Date: 4/5/2022
  Time: 4:17 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sign In</title>
    <link rel="stylesheet" type="text/css" href="stylesheet.css"/>
</head>
<body>
<h2>Ogden-Weber Technical College</h2>
<p>Information Technology Program</p>
<br/>
<img src="images/owatc.png" alt="Ogden-Weber Tech Logo" class="center">
<br/>
<h1>Please Sign In</h1>
<fieldset id="Login">
    <legend>Sign Up!</legend>

    <form action="submitLogin" method="post">
        <label class="top" for="Lusername">Username*: <input type="text" name="Lusername" required
                                             id="Lusername"    title="Enter username" ></label>
        <br>
        <!--Password-->
        <label class="top" for="Lpass">Password*: <input id="Lpass" type="password" name="Lpass" required
        ></label>
        <!-- An element to toggle between password visibility -->
        <input type="checkbox" onclick="showPasswd()">Show Password
        <br>

        <input class="submitBtn-login" type="submit" value="Login" >
    </form>
</fieldset>

<footer>

</footer>
<script src="js/basic.js"></script>
</body>
</html>