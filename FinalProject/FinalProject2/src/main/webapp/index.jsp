<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="stylesheet.css"/>
    <title>Library Management</title>
</head>
<body>
<h1>Welcome to the Library</h1>
<h2>Ogden-Weber Technical College</h2>
<p>Information Technology Program</p>
<br/>
<img src="images/owatc.png" alt="Ogden-Weber Tech Logo" class="center">
<br/>
<p>
    <a href="Library.jsp">Start Web App</a>
</p>

<footer>

</footer>
<script src="js/basic.js"></script>
</body>

</html>