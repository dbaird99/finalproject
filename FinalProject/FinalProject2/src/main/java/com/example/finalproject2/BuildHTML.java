package com.example.finalproject2;

import java.util.List;

public class BuildHTML {

    public String buildCheckedOutForm(List<BooksEntity> books, List<CheckedoutEntity> checkedOuts,
                                      List<ReturnedEntity> returns){

        String form = "<form method='post' action='bookshelf'>";
        form += "<label for='bookID'>Books</label>";
        form += "<select name='bookID' id='bookID'>";
        for (BooksEntity book:
             books) {

            Boolean checked = false;
            int checkedID = -3;

            for (CheckedoutEntity checkedout:
                 checkedOuts) {

                if(book.getBid() == checkedout.getbId()){
                    checked = true;
                    checkedID = checkedout.getCheckedoutId();
                }

            }

            if(checked){

                Boolean returnedB = false;
                ReturnedEntity r = new ReturnedEntity();


                for (ReturnedEntity returned:
                     returns) {

                    if(book.getBid() == returned.getbId()){
                        returnedB = true;
                        r = returned;
                    }

                }

                if(returnedB){

                    CheckedoutEntity c = checkedOuts.get(checkedID);
                    if(r.getReturnedDate().compareTo(c.getCheckedoutDate()) >= 0 ){
                        form += "<option value='" + book.getBid() + "'>" + book.getBookName() + "</option>";
                    }
                }

            }
            else{
                form += "<option value='" + book.getBid() + "'>" + book.getBookName() + "</option>";
            }



        }
        form += "</select>";
        form += "<input type='submit' name='submit' value='Check Out'>";
        form += "<input type='hidden' name='action' value='checkOut'>";
        form += "</form>";

        return form;
    }

    public String buildReturnsForm(List<CheckedoutEntity> checkedOuts, List<ReturnedEntity> returns,
                                   DatabaseFunctions dbF ){

        if(checkedOuts == null){
            return "<p>No books have been checked out yet</p>";
        }

        String form = "<form method='post' action='bookshelf'>";
        form += "<label for='bookID'>Books</label>";
        form += "<select name='bookID' id='bookID'>";
        for (CheckedoutEntity checkedOut:
             checkedOuts) {

            Boolean returnedB = false;
            ReturnedEntity r =new ReturnedEntity();

            for (ReturnedEntity returned:
                 returns) {


                if(checkedOut.getbId() == returned.getbId()){

                    returnedB = true;
                    r = returned;

                }

            }

            if(returnedB){
                if(checkedOut.getCheckedoutDate().compareTo(r.getReturnedDate()) >= 0){
                    form += "<option value='" + checkedOut.getbId() + "'>"
                            + dbF.getBookByID(checkedOut.getbId()) + "</option>";
                }
            }
            else{
                form += "<option value='" + checkedOut.getbId() + "'>"
                        + dbF.getBookByID(checkedOut.getbId()) + "</option>";
            }

        }
        form += "</select>";
        form += "<input type='submit' name='submit' value='Return'>";
        form += "<input type='hidden' name='action' value='returns'>";
        form += "</form>";

        return form;
    }

}
