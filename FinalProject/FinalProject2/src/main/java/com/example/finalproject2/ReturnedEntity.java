package com.example.finalproject2;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "returned", schema = "library", catalog = "")
public class ReturnedEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "returned_id")
    private int returnedId;
    @Basic
    @Column(name = "bID")
    private int bId;
    @Basic
    @Column(name = "returned_date")
    private Date returnedDate;
    /*@ManyToOne
    @JoinColumn(name = "bID", referencedColumnName = "bid", nullable = false)
    private BooksEntity booksByBId;*/

    public int getReturnedId() {
        return returnedId;
    }

    public void setReturnedId(int returnedId) {
        this.returnedId = returnedId;
    }

    public int getbId() {
        return bId;
    }

    public void setbId(int bId) {
        this.bId = bId;
    }

    public Date getReturnedDate() {
        return returnedDate;
    }

    public void setReturnedDate(Date returnedDate) {
        this.returnedDate = returnedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReturnedEntity that = (ReturnedEntity) o;

        if (returnedId != that.returnedId) return false;
        if (bId != that.bId) return false;
        if (returnedDate != null ? !returnedDate.equals(that.returnedDate) : that.returnedDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = returnedId;
        result = 31 * result + bId;
        result = 31 * result + (returnedDate != null ? returnedDate.hashCode() : 0);
        return result;
    }

    /*public BooksEntity getBooksByBId() {
        return booksByBId;
    }

    public void setBooksByBId(BooksEntity booksByBId) {
        this.booksByBId = booksByBId;
    }*/
}
