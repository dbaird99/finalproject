package com.example.finalproject2;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "SignIn_Servlet", urlPatterns ={"/SignIn_Servlet.html"}) //value = "/SignIn_Servlet.html"
public class SignIn_Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<!DOCTYPE html>\n" +
                "<head>\n" +
                "<title>Choose One</title>\n" +
                "</head>\n" +
                "<body>\n");

        //Start of Body

        out.println("" +
                "<h1>Please Sign In\n" +
                "<fieldset id=\"Login\">\n" +
                "          <legend>Login</legend>\n" +
                "          <form action=\"submitLogin\" method=\"post\">\n" +
                "            <label class=\"top\">Username*: <input type=\"text\" name=\"Lusername\" required\n" +
                "                                id=\"userLog\"                          title=\"Enter username\" ></label>\n" +
                "            <label class=\"top\">Password*: <input type=\"password\" name=\"Lpass\" required\n" +
                "                                                 title=\"Put your Password\" ></label>\n" +
                "\n" +
                "              <input  class=\"submitBtn-login\" type=\"submit\" value=\"Login\">\n" +
                "          </form>\n" +
                "\n" +
                "        </fieldset>\n");


        //End of body
        out.println("<footer></footer>" +
                "<script src=\"js/basic.js\"></script>\n" +
                "</body>\n" +
                "</html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
