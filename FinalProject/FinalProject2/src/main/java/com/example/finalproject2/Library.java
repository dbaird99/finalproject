package com.example.finalproject2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "library", value = "/library")
public class Library extends HttpServlet {


    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<!DOCTYPE html>\n" +
                "<head>\n" +
                "<title>Choose One</title>\n" +
                "</head>\n" +
                "<body>\n"

        );

        //Start of Body

        out.println("" +
                "<a href=\"SignIn_Servlet.jsp\" >Sign In</a>\n" +
                "<a href=\"SignUp_Servlet.jsp\" >Sign Up</a>\n"); //onclick="SignIn_Servlet.html"
        //onclick="SignUp_Servlet.html"


        //End of body
        out.println("<footer></footer>" +
                "<script src=\"js/basic.js\"></script>\n" +
                "</body>\n" +
                "</html>");
        //End of HTML

/*        DatabaseFunctions dbF = DatabaseFunctions.getInstance();
        List<BooksEntity> books = dbF.getBooks();

        for (int i = 0; i < books.size(); i++) {
            System.out.println(books.get(i));
            out.println(books.get(i));
        }*/


/*        out.println("<html><body>");
        out.println("<h1>Inventory Management</h1>");
        out.println("<h2>List of Available Books</h2>");
        out.println("<ul>");
        for (BooksEntity book:
            books ) {
            out.println("<li>" + book.getBookName() + "</li>");
        }
        out.println("</ul>");
        out.println("<h2>Book Check-Out</h2>");

        out.println("<h2>Book Returns</h2>");
        out.println("</body></html>");*/
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String action = req.getParameter("action");

        switch (action){
            case "checkOut":

                resp.sendRedirect("library");
                break;
            case "returns":

                resp.sendRedirect("library");
                break;
        }

    }

    public void destroy() {
    }
}