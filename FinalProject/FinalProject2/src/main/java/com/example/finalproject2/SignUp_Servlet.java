package com.example.finalproject2;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import org.hibernate.Session;

@WebServlet(name = "SignUp_Servlet", urlPatterns ={"/SignUp_Servlet.html"}) //value = "/SignUp_Servlet.html"
public class SignUp_Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<!DOCTYPE html>\n" +
                "<head>\n" +
                "<title>Choose One</title>\n" +
                "</head>\n" +
                "<body>\n");

        //Start of Body

        out.println("" +
                "<h1>Welcome to Sign up</h1>\n" +
                "<fieldset id=\"signup\">\n" +
                "        <legend>Sign Up!</legend>\n" +
                "\n" +
                "        <form action=\"SignUp_Servlet.html\" method=\"post\">\n" +
                "          <label class=\"top\">Username*: <input type=\"text\" name=\"username\" required\n" +
                "                                          id=\"userSign\" maxlength=\"30\"    title=\"create username\" ></label>\n" +
                "\n" +
                "          <!--Password 1-->\n" +
                "          <label class=\"top\">Password*: <input id=\"pass1\" type=\"password\" name=\"password1\" required\n" +
                "                                               ></label>\n" +
                "          <!-- An element to toggle between password visibility -->\n" +
                "          <input type=\"checkbox\" onclick=\"showPasswd()\">Show Password\n" +
                "\n" +
                "          <!--Password 2-->\n" +
                "          <label class=\"top\">Confirm Password*: <input id=\"pass2\" type=\"password\" name=\"password2\" required\n" +
                "                                               ></label>\n" +
                "          <!-- An element to toggle between password visibility -->\n" +
                "          <input type=\"checkbox\" onclick=\"showPasswd2()\">Show Password\n" +
                "\n" +
                "          <input class=\"submitBtn-login\" type=\"submit\" value=\"Sign Up!\" >\n" +
                "        </form>\n" +
                "</fieldset>");


        //End of body
        out.println("<footer></footer>" +
                "<script src=\"js/basic.js\"></script>\n" +
                "</body>\n" +
                "</html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //This receives information from the doGet and processes it and sends it back to the doGet or the library get if
        //there is success
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        String username = request.getParameter("username"); //get username
        String password = request.getParameter("password1"); //get password
        int count = 0;

        //process the user.
        UsersEntity user = new UsersEntity();
        DatabaseFunctions dbF = DatabaseFunctions.getInstance();
        List<UsersEntity> users = dbF.getUsers();
        for (int i = 0; i <users.size(); i++) { //loop through to see if there is a matching username
            if (username.equals(users.get(i).getUsername())) {
                out.println( "<script>" +
                        "alert(\" Username Has Already Been Taken. Please Try Again.\");" +
                        "//Go back to the previous page\n" +
                        "window.location.href=\"http://localhost:8080/FinalProject2_war_exploded/SignUp_Servlet.jsp\";"+
                        "</script>");

            }
        }

        if (count == 0) {
            Session session = HibernateUtils.getSessionFactory().openSession();
            session.beginTransaction();
            user.setUsername(username);
            user.setPassword(password);
            session.save(user);
            response.sendRedirect("bookshelf");
            System.out.println("where did you get to?");
            session.getTransaction().commit();
            System.out.println("did you save after commit?");

            //TODO: This may cause errors
            session.close();
            System.out.println("did you save after close?");
        }

        out.println("Did you get here?");

    }



}
