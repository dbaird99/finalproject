package com.example.finalproject2;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "submitLogin", urlPatterns ={"/submitLogin"}) //value = "/submitLogin.html"

public class submitLogin extends HttpServlet {
        public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

            DatabaseFunctions dbF = DatabaseFunctions.getInstance();

            response.setContentType("text/html");
            PrintWriter out = response.getWriter();

            String n=request.getParameter("Lusername");
            String p=request.getParameter("Lpass");


            if(dbF.verifyLogin(n, p)){
                response.sendRedirect("bookshelf");
            }
            else{
                out.print("Sorry, your username or password are incorrect");
                out.print("<a href='SignIn_Servlet.jsp'>Return to Login</a>");
            }

            out.close();
        }
    }


