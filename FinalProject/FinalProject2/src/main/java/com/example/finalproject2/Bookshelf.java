package com.example.finalproject2;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.List;

@WebServlet(name = "Bookshelf", value = "/bookshelf")
public class Bookshelf extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        DatabaseFunctions dbF = DatabaseFunctions.getInstance();
        BuildHTML buildHTML = new BuildHTML();
        List<BooksEntity> books = dbF.getBooks();
        List<CheckedoutEntity> checkedOuts = dbF.getCheckedOut();
        List<ReturnedEntity> returns =dbF.getReturns();
        String checkOutForm = buildHTML.buildCheckedOutForm(books, checkedOuts, returns);
        String returnForm = buildHTML.buildReturnsForm(checkedOuts, returns, dbF);

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheet.css\"/></head><body>");
        out.println("<h1>Inventory Management</h1>");
        out.println("<h2>List of Available Books</h2>");
        out.println("<ul>");
        for (BooksEntity book:
                books ) {
            out.println("<li>" + book.getBookName() + "</li>");
        }
        out.println("</ul>");
        out.println("<h2>Book Check-Out</h2>");
        out.println(checkOutForm);
        out.println("<h2>Book Returns</h2>");
        out.println(returnForm);
        out.println("<hr>");
        out.println("<a href='index.jsp'>Log Out</a>");
        out.println("</body></html>");


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String action = request.getParameter("action");
        DatabaseFunctions dbF = DatabaseFunctions.getInstance();
        Integer bookID;
        java.util.Date date = new java.util.Date();
        Date today = new Date(date.getTime());

        switch (action){
            case "checkOut":
                bookID = Integer.parseInt(request.getParameter("bookID"));
                CheckedoutEntity newCheckOut = new CheckedoutEntity();
                newCheckOut.setbId(bookID);
                newCheckOut.setCheckedoutDate(today);
                try{
                    dbF.insertCheckedOut(newCheckOut);
                }
                catch (Exception e){
                    e.printStackTrace();
                    response.setContentType("text/html");
                    PrintWriter out = response.getWriter();
                    out.println("<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheet.css\"/></head><body>");
                    out.println("<p>Sorry, there was an issue checking out the the book. Please try again.</p>");
                    out.println("<a href='bookshelf'>Return to Bookshelf</a>");
                    out.println("</body></html>");
                    return;
                }
                response.sendRedirect("bookshelf");
                break;
            case "returns":
                bookID = Integer.parseInt(request.getParameter("bookID"));
                ReturnedEntity newReturn = new ReturnedEntity();
                newReturn.setbId(bookID);
                newReturn.setReturnedDate(today);
                try{
                    dbF.insertReturn(newReturn);
                }
                catch (Exception e){
                    e.printStackTrace();
                    response.setContentType("text/html");
                    PrintWriter out = response.getWriter();
                    out.println("<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheet.css\"/></head><body>");
                    out.println("<p>Sorry, there was an issue returning the the book. Please try again.</p>");
                    out.println("<a href='bookshelf'>Return to Bookshelf</a>");
                    out.println("</body></html>");
                    return;
                }
                response.sendRedirect("bookshelf");
                break;
        }

    }
}
