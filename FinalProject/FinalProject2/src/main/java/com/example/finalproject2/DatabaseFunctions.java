/* This program is a part of a simple hibernate example used for CIT-360
   It is written by Troy Tuckett.  BYUI.EDU
 */
package com.example.finalproject2;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/** TestDAO implemented using a singleton pattern
 *  Used to get city data from my MYSQL database*/
public class DatabaseFunctions {

    SessionFactory factory = null;
    Session session = null;

    private static DatabaseFunctions single_instance = null;

    private DatabaseFunctions()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static DatabaseFunctions getInstance()
    {
        if (single_instance == null) {
            single_instance = new DatabaseFunctions();
        }

        return single_instance;
    }

    public List<BooksEntity> getBooks(){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "from com.example.finalproject2.BooksEntity";
            List<BooksEntity> books = (List<BooksEntity>)session.createQuery(query).getResultList();
            session.getTransaction().commit();
            return books;
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
        finally {
            session.close();
        }

    }

    public List<CheckedoutEntity> getCheckedOut(){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "FROM CheckedoutEntity ";
            return session.createQuery(query, CheckedoutEntity.class).getResultList();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
        finally {
            session.close();
        }

    }

    public List<ReturnedEntity> getReturns(){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "FROM ReturnedEntity ";
            return session.createQuery(query, ReturnedEntity.class).getResultList();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
        finally {
            session.close();
        }

    }

    public String getBookByID(int bookID){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "FROM BooksEntity WHERE bid = " + bookID;
            BooksEntity book = session.createQuery(query, BooksEntity.class).getSingleResult();
            return book.getBookName();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
            return "";
        }
        finally {
            session.close();
        }

    }

    public List<UsersEntity> getUsers(){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "from com.example.finalproject2.UsersEntity";
            List<UsersEntity> users = (List<UsersEntity>)session.createQuery(query).getResultList();
            session.getTransaction().commit();
            return users;
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
        finally {
            session.close();
        }

    }

    /*public UsersEntity getUserByID(int userID){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "From com.example.finalproject2.UsersEntity WHERE uid = " + Integer.toString(userID);
            UsersEntity user = (UsersEntity) session.createQuery(query).getSingleResult();
            session.getTransaction().commit();
            return user;
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
        finally {
            session.close();
        }

    }*/

    public Boolean verifyLogin(String n, String p){

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String query = "FROM UsersEntity WHERE username = '" + n + "'";
            UsersEntity user = session.createQuery(query, UsersEntity.class).getSingleResult();
            if(user != null) {
                if (user.getPassword().equals(p)) {
                    return true;
                }
            }
            return false;
        }
        catch (Exception e){
            session.getTransaction().rollback();
            e.printStackTrace();
            //return false;
            return false;
        }
        finally {
            session.close();
        }

    }

    public void insertCheckedOut(CheckedoutEntity checkOut) throws Exception{

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            session.save(checkOut);
            session.getTransaction().commit();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            throw e;
        }
        finally {
            session.close();
        }

    }

    public void insertReturn(ReturnedEntity newReturn)throws Exception{

        try{
            session = factory.openSession();
            session.getTransaction().begin();
            session.save(newReturn);
            session.getTransaction().commit();
        }
        catch (Exception e){
            session.getTransaction().rollback();
            throw e;
        }
        finally {
            session.close();
        }

    }

}
