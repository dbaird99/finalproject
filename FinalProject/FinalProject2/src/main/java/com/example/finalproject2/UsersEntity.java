package com.example.finalproject2;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "users", schema = "library", catalog = "")
public class UsersEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "UID")
    private int uid;
    @Basic
    @Column(name = "USERNAME")
    private String username;
    @Basic
    @Column(name = "PASSWORD")
    private String password;
    @Basic
    @Column(name = "books")
    private String books;
    @Basic
    @Column(name = "checkedout")
    private Date checkedout;
    @Basic
    @Column(name = "returned")
    private Date returned;
    @Basic
    @Column(name = "book2")
    private String book2;
    @Basic
    @Column(name = "checkedout2")
    private Date checkedout2;
    @Basic
    @Column(name = "returned2")
    private Date returned2;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBooks() {
        return books;
    }

    public void setBooks(String books) {
        this.books = books;
    }

    public Date getCheckedout() {
        return checkedout;
    }

    public void setCheckedout(Date checkedout) {
        this.checkedout = checkedout;
    }

    public Date getReturned() {
        return returned;
    }

    public void setReturned(Date returned) {
        this.returned = returned;
    }

    public String getBook2() {
        return book2;
    }

    public void setBook2(String book2) {
        this.book2 = book2;
    }

    public Date getCheckedout2() {
        return checkedout2;
    }

    public void setCheckedout2(Date checkedout2) {
        this.checkedout2 = checkedout2;
    }

    public Date getReturned2() {
        return returned2;
    }

    public void setReturned2(Date returned2) {
        this.returned2 = returned2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsersEntity that = (UsersEntity) o;

        if (uid != that.uid) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (books != null ? !books.equals(that.books) : that.books != null) return false;
        if (checkedout != null ? !checkedout.equals(that.checkedout) : that.checkedout != null) return false;
        if (returned != null ? !returned.equals(that.returned) : that.returned != null) return false;
        if (book2 != null ? !book2.equals(that.book2) : that.book2 != null) return false;
        if (checkedout2 != null ? !checkedout2.equals(that.checkedout2) : that.checkedout2 != null) return false;
        if (returned2 != null ? !returned2.equals(that.returned2) : that.returned2 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uid;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (books != null ? books.hashCode() : 0);
        result = 31 * result + (checkedout != null ? checkedout.hashCode() : 0);
        result = 31 * result + (returned != null ? returned.hashCode() : 0);
        result = 31 * result + (book2 != null ? book2.hashCode() : 0);
        result = 31 * result + (checkedout2 != null ? checkedout2.hashCode() : 0);
        result = 31 * result + (returned2 != null ? returned2.hashCode() : 0);
        return result;
    }
}
