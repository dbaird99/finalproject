package com.example.finalproject2;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "checkedout", schema = "library", catalog = "")
public class CheckedoutEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "checkedout_id")
    private int checkedoutId;
    @Basic
    @Column(name = "bID")
    private int bId;
    @Basic
    @Column(name = "checkedout_date")
    private Date checkedoutDate;
    /*@ManyToOne
    @JoinColumn(name = "bID", referencedColumnName = "bid", nullable = false)
    private BooksEntity booksByBId;*/

    public int getCheckedoutId() {
        return checkedoutId;
    }

    public void setCheckedoutId(int checkedoutId) {
        this.checkedoutId = checkedoutId;
    }

    public int getbId() {
        return bId;
    }

    public void setbId(int bId) {
        this.bId = bId;
    }

    public Date getCheckedoutDate() {
        return checkedoutDate;
    }

    public void setCheckedoutDate(Date checkedoutDate) {
        this.checkedoutDate = checkedoutDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CheckedoutEntity that = (CheckedoutEntity) o;

        if (checkedoutId != that.checkedoutId) return false;
        if (bId != that.bId) return false;
        if (checkedoutDate != null ? !checkedoutDate.equals(that.checkedoutDate) : that.checkedoutDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = checkedoutId;
        result = 31 * result + bId;
        result = 31 * result + (checkedoutDate != null ? checkedoutDate.hashCode() : 0);
        return result;
    }

    /*public BooksEntity getBooksByBId() {
        return booksByBId;
    }

    public void setBooksByBId(BooksEntity booksByBId) {
        this.booksByBId = booksByBId;
    }*/
}
